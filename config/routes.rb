Rails.application.routes.draw do
  resources :categories do
    resources :posts
  end

  resources :posts do
    resources :comments
  end

  root to: 'categories#index'
end
