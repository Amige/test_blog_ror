class PostsController < ApplicationController
  def index
  end

  def new
    @category = Category.find(params[:category_id])
  end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    @category = Category.find(params[:category_id])
    @post = @category.posts.create(post_params)
    redirect_to category_path(@category)
  end

  def update
    @post = Post.find(params[:id])

    puts post_params.inspect
    if @post.update(post_params)
      redirect_to category_path(@post.category)
    else
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @category = @post.category
    @post.destroy

    redirect_to category_path(@category)
  end

  private
  def post_params
    params.require(:post).permit(:name, :content, :file)
  end
end
