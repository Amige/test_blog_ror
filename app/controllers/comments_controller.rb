class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comment_params)
    redirect_to category_path(@post.category)
  end

  private
  def comment_params
    params.require(:comment).permit(:author, :content)
  end
end
