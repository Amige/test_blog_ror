class Category < ApplicationRecord
  has_many :posts, dependent: :destroy

  validates :name, presence: true, format: {with: /\A[A-Z]+[a-zA-Z]*\s+[a-zA-Z]{2,}+[.]\z/, message: "wrong format of name"}
end
