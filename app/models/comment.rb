class Comment < ApplicationRecord
  belongs_to :post

  validates :author, presence: true, format: { with: /\A[A-Z]+[a-zA-Z]*\s+[A-Z]+[a-zA-Z]+[.]\z/, message: "wrong format of name" }
end
