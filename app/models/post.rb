class Post < ApplicationRecord
  has_many :comments, dependent: :destroy
  belongs_to :category

  mount_uploader :file, FileUploader

  validates :name, presence: true, format: {with: /\A[A-Z]+[a-zA-Z]*\s+[a-zA-Z]{2,}+[.]\z/, message: "wrong format of name"}
  validates :file, file_size: {less_than: 2.megabytes}
end
